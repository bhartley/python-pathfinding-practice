# -*- coding: utf-8 -*-

default_damage = 20
default_move_random = False
default_move_threshold = 0.25

class Critter():
    def __init__(self, name, pos):
        self.name = name
        self.pos = pos
        self.move_threshold = default_move_threshold
        self.move_random = default_move_random
        self.damage = default_damage

    def get_move_threshold(self):
        return self.move_threshold

    def set_position(self, pos):
        self.pos = pos

    def get_position(self):
        return self.pos
