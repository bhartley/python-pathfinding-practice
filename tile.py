# -*- coding: utf-8 -*-

class Tile():
    def __init__(self, x, y, z, material):
        self.pos = (x, y, z)
        self.material = material
        self.tagged = False
        self.adjacent_tiles = []

    def current_material_name(self):
        return self.material['name']

    def is_empty(self):
        if self.material['name'] == 'empty':
            return True
        else:
            return False

def is_connected(room_one, room_two):
    x_diff = abs(room_one[0] - room_two[0])
    y_diff = abs(room_one[1] - room_two[1])
    z_diff = abs(room_one[2] - room_two[2])

    if x_diff == 1 and y_diff == 0 and z_diff == 0:
        return True
    elif x_diff == 0 and y_diff == 1 and z_diff == 0:
        return True
    elif x_diff == 0 and y_diff == 0 and z_diff == 1:
        return True
    else:
        return False