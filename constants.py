# -*- coding: utf-8 -*-

_EMPTY = 0
_DIRT = 1
_ROCK = 2
_WATER = 3
_ICE = 4

material_list = { '_EMPTY': _EMPTY, '_DIRT': _DIRT, '_ROCK': _ROCK, '_WATER': _WATER, '_ICE': _ICE }

BROWN = (144, 72, 0)
BROWN_TAGGED = (180, 90, 10)
LIGHT_BLUE = (50, 50, 255)
LIGHT_BLUE_TAGGED = (72, 72, 255)
BLACK = (5, 5, 5)
LIGHT_GREY = (77, 77, 77)
GREY = (55, 55, 55)
DARK_GREY = (30, 30, 30)
DARK_GREY_TAGGED = (80, 80, 80)
GOLD = (255, 215, 0)
ORANGE = (255, 165, 0)
GREEN = (0, 200, 32)
GREEN_TAGGED = (33, 238, 66)
PURPLE = (66, 0, 144)
PURPLE_TAGGED = (93, 25, 180)

colors = { 'BROWN': BROWN, 'BROWN_TAGGED': BROWN_TAGGED, 'LIGHT_BLUE': LIGHT_BLUE, 'LIGHT_BLUE_TAGGED': LIGHT_BLUE_TAGGED, 'LIGHT_GREY': LIGHT_GREY, 'BLACK': BLACK, 'GOLD': GOLD, 'ORANGE': ORANGE, 'GREY': GREY, 'DARK_GREY': DARK_GREY, 'DARK_GREY_TAGGED': DARK_GREY_TAGGED, 'GREEN': GREEN, 'GREEN_TAGGED': GREEN_TAGGED, 'PURPLE': PURPLE, 'PURPLE_TAGGED': PURPLE_TAGGED }

EMPTY = { 'color': DARK_GREY, 'color_tagged': DARK_GREY_TAGGED, 'name': 'empty', 'index': '_EMPTY' }
DIRT = { 'color': BROWN, 'color_tagged': BROWN_TAGGED, 'name': 'dirt', 'index': '_DIRT' }
ROCK = { 'color': GREEN, 'color_tagged': GREEN_TAGGED, 'name': 'rock', 'index': '_ROCK' }
WATER = { 'color': LIGHT_BLUE, 'color_tagged': LIGHT_BLUE_TAGGED, 'name': 'water', 'index': '_WATER' }
ICE = { 'color': PURPLE, 'color_tagged': PURPLE_TAGGED, 'name': 'ice', 'index': '_ICE', 'effect': 'slow' }

materials = { _EMPTY: EMPTY, _DIRT: DIRT, _ROCK: ROCK, _WATER: WATER, _ICE: ICE }
