# -*- coding: utf-8 -*-

import math, random

from pygame.locals import K_d, K_f, K_UP, K_LEFT, K_DOWN, K_RIGHT
from constants import EMPTY
from tile import Tile
from critter import Critter
from utils import random_material, random_non_empty_material

min_dim = 3
max_dim = 6

class Board():
    def __init__(self, x_size, y_size, z_size, tile_size, critters):
        if x_size < min_dim:
            self.x = min_dim
        elif x_size > max_dim:
            self.x = max_dim
        else:
            self.x = int(x_size)
        if y_size < min_dim:
            self.y = min_dim
        elif y_size > max_dim:
            self.y = max_dim
        else:
            self.y = int(y_size)
        if z_size < min_dim:
            self.z = min_dim
        elif z_size > max_dim:
            self.z = max_dim
        else:
            self.z = int(x_size)

        self.tile_size = tile_size
        self.empty_board()
        
        self.seed_board(get_random_coordinate(self.x - 1, self.y - 1, self.z - 1))
        self.player_alive = True
        self.player_effect = None
        self.score = 0
        self.player_life = 100

        self.critters = []
        for n in range(0, critters):
            self.create_critter('critter ' + str(n))

    def hurt_player(self, dmg):
        self.player_life -= dmg
        if self.player_life <= 0:
            self.player_alive = False

    def heal_player(self, points):
        self.player_life += points

    def empty_board(self):
        self.board = []
        for i in range(0, self.x):
            tmp = []
            for j in range(0, self.y):
                tempest = []
                for k in range(0, self.z):
                    new_tile = Tile(i, j, k, EMPTY)
                    tempest.append(new_tile)
                tmp.append(tempest)
            self.board.append(tmp)

    def seed_board(self, sd):
        self.randomize_floor_at_coordinate(sd)
        current_total_rooms = 1
        self.player_pos = sd
        new_sd = tuple(sd)

        min_rooms = int((self.x * self.y * self.z) * 0.25)
        max_rooms = int((self.x * self.y * self.z) * 0.33)
        rooms = random.randint(min_rooms, max_rooms)
        print('generating ' + str(rooms) + ' rooms. Min: ' + str(min_rooms) + ', max: ' + str(max_rooms))
        while current_total_rooms < rooms:
            rand = random.randint(0, 2) # x, y, z coordinates
            change = int(math.pow(-1, random.randint(1, 2))) # either 1 or -1
            tmp_x, tmp_y, tmp_z = new_sd[0], new_sd[1], new_sd[2]
            if rand == 0:
                print('x, ' + str(tmp_x + change))
                if not (tmp_x + change > self.x - 1 or tmp_x + change < 0):
                    tmp_x += change
            elif rand == 1:
                print('y, ' + str(tmp_y + change))
                if not (tmp_y + change > self.y - 1 or tmp_y + change < 0):
                    tmp_y += change
            elif rand == 2:
                print('z, ' + str(tmp_z + change))
                if not (tmp_z + change > self.z - 1 or tmp_z + change < 0):
                    tmp_z += change
            new_sd = (tmp_x, tmp_y, tmp_z)
            print(new_sd)
            if self.board[new_sd[0]][new_sd[1]][new_sd[2]].is_empty():
                self.randomize_floor_at_coordinate(new_sd)
                current_total_rooms += 1

    def randomize_board(self):
        self.board = []
        for i in range(0, self.x):
            tmp = []
            for j in range(0, self.y):
                tempest = []
                for k in range(0, self.z):
                    new_tile = Tile(i, j, k, random_material())
                    tempest.append(new_tile)
                tmp.append(tempest)
            self.board.append(tmp)

    def randomize_floor_at_coordinate(self, sd):
        empty = False
        if self.board[sd[0]][sd[1]][sd[2]].is_empty():
            empty = True
        new_material = random_non_empty_material()
        self.board[sd[0]][sd[1]][sd[2]].material = new_material
            
        if empty:
            potentially_adjacent_rooms = adj_room_combos(sd, self.x - 1, self.y - 1, self.z - 1)
            for x in range(0, len(potentially_adjacent_rooms)):
                p = potentially_adjacent_rooms[x]
                if self.board[p[0]][p[1]][p[2]].is_empty():
                    continue
                else:
                    self.board[sd[0]][sd[1]][sd[2]].adjacent_tiles.append(p)
                    self.board[p[0]][p[1]][p[2]].adjacent_tiles.append(sd)

    def handle_move(self, key):
        x_pos, y_pos, z_pos = self.player_pos[0], self.player_pos[1], self.player_pos[2]

        if key == K_UP:
            y_pos -= 1
            if y_pos < 0:
                return
        elif key == K_LEFT:
            x_pos -= 1
            if x_pos < 0:
                return
        elif key == K_DOWN:
            y_pos += 1
            if y_pos > self.y - 1:
                return
        elif key == K_RIGHT:
            x_pos += 1
            if x_pos > self.x - 1:
                return
        elif key == K_d:
            z_pos += 1
            if z_pos > self.z - 1:
                return
        elif key == K_f:
            z_pos -= 1
            if z_pos < 0:
                return

        # move the player if able
        if self.check_move(x_pos, y_pos, z_pos):
            self.set_player_position(x_pos, y_pos, z_pos)
            self.handle_new_player_position()
            self.score += 10

            # now check all the critters and maybe move them
            for i in range(0, len(self.critters)):
                thresh = self.critters[i].get_move_threshold()

                if self.player_effect == "slow":
                    thresh = thresh * 2

                if random.random() < thresh:
                    if self.critters[i].move_random:
                        self.critter_move_random(i)
                    else:
                        self.critter_move_toward(i, self.player_pos)

    def move_player_to_random_tile(self):
        available_rooms = self.get_non_empty_rooms()
        available_rooms.remove(self.player_pos)
        for x in range(0, len(self.critters)):
            pos = self.critters[x].get_position()
            if pos in available_rooms:
                available_rooms.remove(pos)
        room = random.choice(available_rooms)
        self.set_player_position(room[0], room[1], room[2])

    def get_legal_moves_for_tile(self, x, y, z):
        x_max, y_max, z_max = self.x - 1, self.y - 1, self.z - 1
        moves = []

        tmp_x = x - 1
        if not tmp_x < 0 and not self.board[tmp_x][y][z].is_empty():
            tuple_x = (tmp_x, y, z)
            moves.append(tuple_x)

        tmp_x = x + 1
        if not tmp_x > x_max and not self.board[tmp_x][y][z].is_empty():
            tuple_x = (tmp_x, y, z)
            moves.append(tuple_x)

        tmp_y = y - 1
        if not tmp_y < 0 and not self.board[x][tmp_y][z].is_empty():
            tuple_y = (x, tmp_y, z)
            moves.append(tuple_y)

        tmp_y = y + 1
        if not tmp_y > y_max and not self.board[x][tmp_y][z].is_empty():
            tuple_y = (x, tmp_y, z)
            moves.append(tuple_y)

        tmp_z = z - 1
        if not tmp_z < 0 and not self.board[x][y][tmp_z].is_empty():
            tuple_z = (x, y, tmp_z)
            moves.append(tuple_z)

        tmp_z = z + 1
        if not tmp_z > z_max and not self.board[x][y][tmp_z].is_empty():
            tuple_z = (x, y, tmp_z)
            moves.append(tuple_z)

        return moves

    def check_move(self, x, y, z):
        if self.board[x][y][z].is_empty():
            return False
        else:
            return True

    def set_player_position(self, x, y, z):
        x_min_pos, y_min_pos, z_min_pos = 0, 0, 0
        x_max_pos, y_max_pos, z_max_pos = self.x - 1, self.y - 1, self.z - 1
        if x < x_min_pos:
            x = x_min_pos
        elif x > x_max_pos:
            x = x_max_pos
        if y < y_min_pos:
            y = y_min_pos
        elif y > y_max_pos:
            y = y_max_pos
        if z < z_min_pos:
            z = z_min_pos
        elif z > z_max_pos:
            z = z_max_pos

        if self.check_move(x, y, z):
            self.player_pos = (x, y, z)
            self.board[x][y][z].tagged = True

    def handle_new_player_position(self):
        pos = self.player_pos
        mat = self.board[pos[0]][pos[1]][pos[2]].material
        effect = None
        if "effect" in mat.keys():
            effect = mat["effect"]
        self.player_effect = effect

    def get_non_empty_rooms(self):
        rooms = []
        for x in range(0, len(self.board)):
            for y in range(0, len(self.board[x])):
                for z in range(0, len(self.board[x][y])):
                    if not self.board[x][y][z].is_empty():
                        rooms.append((x, y, z))
        return rooms

    def check_for_win(self):
        ok = True
        for x in range(0, len(self.board)):
            for y in range(0, len(self.board[x])):
                for z in range(0, len(self.board[x][y])):
                    if self.board[x][y][z].current_material_name() != "empty" and self.board[x][y][z].tagged == False:
                        ok = False
                        break
                if ok == False:
                    break
            if ok == False:
                break
        return ok

    def create_critter(self, name):
        empty = True
        while empty:
            tmp = get_random_coordinate(self.x - 1, self.y - 1, self.z - 1)
            if not self.board[tmp[0]][tmp[1]][tmp[2]].is_empty():
                crit = Critter(name, tmp)
                self.critters.append(crit)
                empty = False

    def critter_move_random(self, index):
        x_pos, y_pos, z_pos = self.critters[index].pos[0], self.critters[index].pos[1], self.critters[index].pos[2]
        moves = self.get_legal_moves_for_tile(x_pos, y_pos, z_pos)
        move = random.choice(moves)
        self.critters[index].set_position(move)

    def critter_move_toward(self, index, pos):
        paths = self.get_paths(self.critters[index].pos, pos)
        print('total paths: ' + str(len(paths)))
        current_length = len(paths[0])
        current_index = 0
        if len(paths) > 1:
            for x in range(0, len(paths)):
                if len(paths[x]) < current_length:
                    current_length = len(paths[x])
                    current_index = x
                    break
        print('chosen path: ' + str(paths[current_index]))
        if len(paths[current_index]) > 1:
            move = paths[current_index][1]
            self.critters[index].set_position(move)

    def get_paths(self, a, b):
        paths = []
        candidate_paths = [[a]]
        current_shortest_path_length = -1

        # while there are still candidate paths
        while len(candidate_paths) > 0:
            print(len('candidate paths: ' + str(candidate_paths)))
            # examine each remaining candidate path
            new_candidate_paths = []
            for p in range(0, len(candidate_paths)):
                # see what its last move was
                current_path = list(candidate_paths[p])
                last_move = current_path[len(current_path) - 1]
                # if the last move arrived at b add the path to the paths list and remove it from the candidates
                if last_move == b and current_path not in paths:
                    if current_shortest_path_length == -1:
                        paths.append(current_path)
                        current_shortest_path_length = len(current_path)
                    elif len(current_path) < len(paths[0]):
                        paths = [current_path]
                else:
                    available_moves = self.get_legal_moves_for_tile(last_move[0], last_move[1], last_move[2])
                    if len(available_moves) == 0:
                        # dead path
                        continue
                    elif len(available_moves) == 1:
                        # 1 move, add it onto the current path and carry on
                        if not (available_moves[0]) in current_path:
                            current_path.append(available_moves[0])
                            if len(current_path) < current_shortest_path_length or current_shortest_path_length == -1:
                                new_candidate_paths.append(current_path)
                    else:
                        # multiple moves, split current_path
                        tmp = list(current_path)
                        for x in range(0, len(available_moves)):
                            if not available_moves[x] in tmp:
                                copy = list(tmp)
                                copy.append(available_moves[x])
                                if len(copy) < current_shortest_path_length or current_shortest_path_length == -1:
                                    new_candidate_paths.append(copy)
            candidate_paths = clean_paths(list(new_candidate_paths))
        return paths

def get_random_coordinate(x_max, y_max, z_max):
    x = random.randint(0, x_max)
    y = random.randint(0, y_max)
    z = random.randint(0, z_max)
    print(x, y, z, x_max, y_max, z_max)
    return (x, y, z)

def adj_room_combos(p, max_x, max_y, max_z):
    rooms = []
    x_0, y_0, z_0 = p[0], p[1], p[2]

    # possible x changes
    x_1 = x_0 + 1
    if not x_1 > max_x:
        rooms.append((x_1, y_0, z_0))

    x_2 = x_0 - 1
    if not x_2 < 0:
        rooms.append((x_2, y_0, z_0))

    # possible y changes
    y_1 = y_0 + 1
    if not y_1 > max_y:
        rooms.append((x_0, y_1, z_0))

    y_2 = y_0 - 1
    if not y_2 < 0:
        rooms.append((x_0, y_2, z_0))

    # possible z changes
    z_1 = z_0 + 1
    if not z_1 > max_z:
        rooms.append((x_0, y_0, z_1))

    z_2 = z_0 - 1
    if not z_2 < 0:
        rooms.append((x_0, y_0, z_2))

    return rooms

def clean_paths(plist):
    pathlist = list(plist)
    i = 0
    total_paths = len(pathlist)
    while i < total_paths:
        paths_to_pop = []
        for x in range(i + 1, total_paths):
            if are_equal_paths(pathlist[i], pathlist[x]):
                paths_to_pop.append(x)
        for y in range(0, len(paths_to_pop)):
#            print('poppin ' + str(len(pathlist)))
            pathlist.pop(paths_to_pop[len(paths_to_pop) - 1 - y])
            total_paths -= 1
        i += 1
    return pathlist

def are_equal_paths(p_one, p_two):
    if not p_one or not p_two:
        return False

    # start in the same spot
    if not p_one[0] == p_two[0]:
        return False

    # end in the same spot
    if not p_one[len(p_one) - 1] == p_two[len(p_two) - 1]:
        return False

    # same length
    if not len(p_one) == len(p_two):
        return False

    # all p_one coordinates are in p_two
    for x in range(0, len(p_one)):
        if not p_one[x] in p_two:
            return False

    # all p_two coordinates are in p_one
    for y in range(0, len(p_two)):
        if not p_two[y] in p_one:
            return False

    return True
