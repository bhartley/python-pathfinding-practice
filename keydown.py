# -*- coding: utf-8 -*-

from pygame.locals import K_RETURN, K_SPACE

def handle_keydown(event, board):
    key = event.key

    if key == K_RETURN:
        board.create_critter('steve')
    elif key == K_SPACE:
        board.move_player_to_random_tile()
