# -*- coding: utf-8 -*-

import random

import constants

def pause():
    input("Press the <ENTER> key to continue...")

# returns color tuple
def random_material():
    return constants.materials[random.choice(list(constants.materials.keys()))]

def random_non_empty_material():
    empty = True
    tmp = None
    while empty:
        tmp = constants.materials[random.choice(list(constants.materials.keys()))]
        if tmp['name'] != "empty":
            empty = False
    return tmp
