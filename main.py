#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb 10 19:15:07 2018

@author: Benjamin Hartley

"""

import pygame, sys, time
from pygame.locals import K_d, K_f, K_UP, K_LEFT, K_DOWN, K_RIGHT, KEYDOWN, QUIT
from utils import pause
from constants import colors
from board import Board
from keydown import handle_keydown as kd

# Configure game size here or parametrize it and add argument options
x_tiles, y_tiles, z_tiles = 4, 4, 4
default_tile_size = 80
default_critters = 2
game_board = Board(x_tiles, y_tiles, z_tiles, default_tile_size, default_critters)

# Figure out how big to make the screen in order to fit the game boards and nothing else
screen_x = ((z_tiles * (x_tiles + 1)) - 1) * default_tile_size
screen_y = (y_tiles * default_tile_size) + ((z_tiles - 1) * default_tile_size)

# Fire up pygame, set up window title and font
pygame.init()
DISP = pygame.display.set_mode((screen_x, screen_y))
default_font = pygame.font.SysFont('monospace', 20)
pygame.display.set_caption('monkeys')

# Draw game board function (todo: move out of main)
def draw_board(board):
    DISP.fill(colors['LIGHT_GREY'])
    zed = board.tile_size
    plr_offset = int(zed / 2) # center the player in the square

    for x in range(0, len(board.board)):

        for y in range(0, len(board.board[x])):

            for z in range(0, len(board.board[x][y])): # each square
                x_offset = int(default_tile_size * (x_tiles + 0.5) * z) # offset for
                y_offset = int(default_tile_size * 0.5 * z)             # next z-level
                col = board.board[x][y][z].material['color']
                if board.board[x][y][z].tagged:
                    col = board.board[x][y][z].material['color_tagged']
                pygame.draw.rect(DISP, col, ((zed * x) + x_offset, zed * y + y_offset, zed, zed))

                # Draw the player
                if board.player_pos == (x, y, z):
                    x_plr = x_offset + plr_offset + (x * zed)
                    y_plr = y_offset + plr_offset + (y * zed)
                    pygame.draw.circle(DISP, colors['GOLD'], (x_plr, y_plr), int(zed / 4), 0)

                # Loop through and draw critters
                for i in range(0, len(board.critters)):
                    pos = board.critters[i].get_position()
                    if pos == (x, y, z):
                        x_crtr = x_offset + plr_offset + (x * zed)
                        y_crtr = y_offset + plr_offset + (y * zed)
                        pygame.draw.circle(DISP, colors['ORANGE'], (x_crtr, y_crtr), int(zed / 5), 0)
                        if board.player_pos == pos:
                            pygame.draw.circle(DISP, colors['BLACK'], (x_crtr, y_crtr), int(zed / 3), 0)
                            board.hurt_player(board.critters[i].damage)
                            if board.player_alive == False:
                                print('player died to a critter')

    scoretext = default_font.render('Score {0}'.format(board.score), 1, colors['LIGHT_BLUE'])
    lifetext = default_font.render('Life {0}'.format(board.player_life), 1, colors['LIGHT_BLUE'])
    DISP.blit(scoretext, (5, int(default_tile_size * (y_tiles + 0.5))))
    DISP.blit(lifetext, (5, int(default_tile_size * (y_tiles + 1))))
    pygame.display.update()

## Set up end game plan
def end_game():
    pause()
    pygame.quit()
    sys.exit()

## Prepare to start the game
draw_board(game_board)

## Go
while game_board.player_alive:
    for event in pygame.event.get():
        if event.type == KEYDOWN and event.key in [K_d, K_f, K_UP, K_LEFT, K_DOWN, K_RIGHT]:
            game_board.handle_move(event.key)
            draw_board(game_board)
        elif event.type == KEYDOWN:
            kd(event, game_board)
            draw_board(game_board)
        elif event.type == QUIT:
            pygame.quit()
            sys.exit()

        if game_board.check_for_win() == True:
            draw_board(game_board)
            print('winner')
            end_game()
    time.sleep(0.1)

pygame.display.update()
end_game()
